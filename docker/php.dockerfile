FROM composer as composer
FROM php:8.1-fpm-alpine

ENV COMPOSER_HOME /opt/var/composer
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

RUN mkdir -p /opt

RUN apk add --no-cache bash

SHELL [ "bash", "-euxo", "pipefail", "-c" ]

RUN apk add --no-cache \
    icu-dev \
    libpq-dev

RUN docker-php-ext-configure intl

RUN docker-php-ext-install  \
      intl \
      pdo \
      pdo_pgsql \
      pgsql

WORKDIR /opt

