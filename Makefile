.PHONY: all
all: .git/hooks/commit-msg

##@ This hook places commitlint as a commit-msg hook
# cf https://www.conventionalcommits.org/en/v1.0.0/
.git/hooks/commit-msg: | .git/hooks
	cp bin/commitlint-verification.sh $@

.PHONY: install-commitlint
install-commitlint:
	npm install --global --save-dev @commitlint/{config-conventional,cli}

.git/hooks:
	mkdir -p $@
